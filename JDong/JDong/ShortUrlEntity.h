//
//  ShortUrlEntity.h
//  JDong
//
//  Created by liunian on 13-12-16.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface ShortUrlEntity : NSManagedObject

@property (nonatomic, retain) NSNumber * itemid;
@property (nonatomic, retain) NSString * shorturl;

@end
