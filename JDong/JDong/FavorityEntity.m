//
//  FavorityEntity.m
//  JDong
//
//  Created by liunian on 13-12-10.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import "FavorityEntity.h"


@implementation FavorityEntity

@dynamic itemID;
@dynamic itemName;
@dynamic sortid;
@dynamic tradingVolumeInThirtyDays;
@dynamic promotionPrice;
@dynamic itemImageURLString;
@dynamic itemClickURLString;
@dynamic itemPrice;

@end
