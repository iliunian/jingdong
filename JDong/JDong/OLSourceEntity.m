//
//  OLSourceEntity.m
//  JDong
//
//  Created by liunian on 13-12-10.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import "OLSourceEntity.h"


@implementation OLSourceEntity

@dynamic sid;
@dynamic themeid;
@dynamic type;
@dynamic desc;
@dynamic name;
@dynamic icon;

@end
