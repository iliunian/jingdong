//
//  CustomSourceEntity.h
//  JDong
//
//  Created by liunian on 13-12-10.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface CustomSourceEntity : NSManagedObject

@property (nonatomic, retain) NSDate * creatime;
@property (nonatomic, retain) NSString * icon;
@property (nonatomic, retain) NSString * keyword;

@end
