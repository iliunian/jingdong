//
//  UserSourceEntity.h
//  JDong
//
//  Created by liunian on 13-12-10.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface UserSourceEntity : NSManagedObject

@property (nonatomic, retain) NSString * desc;
@property (nonatomic, retain) NSString * icon;
@property (nonatomic, retain) NSNumber * isCustom;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * keyword;
@property (nonatomic, retain) NSNumber * sid;
@property (nonatomic, retain) NSNumber * sortid;
@property (nonatomic, retain) NSNumber * themeid;
@property (nonatomic, retain) NSNumber * itemtype;
@property (nonatomic, retain) NSString * url;
@end
