//
//  FeedbackRequest.h
//  JDong
//
//  Created by liunian on 13-12-17.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ReqDelegate.h"

@interface FeedbackRequest : NSObject
@property (nonatomic , assign) id<ReqDelegate>delegate;
- (void)sendContent:(NSString *)content email:(NSString *)email delegate:(id<ReqDelegate>)delegate;
@end
