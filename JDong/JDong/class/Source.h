//
//  Source.h
//  iMiniTao
//
//  Created by liu nian on 13-8-14.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef enum
{
    ITEMTYPENONE = 0,
    ITEMTYPECUZYITEM =1,
    ITEMTYPEKEYWORD =2,
    ITEMTYPESHOP =3,
    ITEMTYPEGROUP =4,
    ITEMTYPEWAP =5,
} ITEMTYPE;
@interface Source : NSObject<NSCoding>
@property (nonatomic, retain) NSString *sid;
@property (nonatomic, retain) NSString *themeid;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *keyword;
@property (nonatomic, retain) NSString *description;
@property (nonatomic, retain) NSString *icon;
@property (nonatomic, retain) NSString *type;
@property (nonatomic, retain) NSString *url;
@property (nonatomic, assign) ITEMTYPE itemtype;

+ (id)createWithDict:(NSDictionary *)dict;
- (id)initWithDict:(NSDictionary *)dict;
@end
