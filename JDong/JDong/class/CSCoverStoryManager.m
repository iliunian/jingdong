//
//  CSCoverStoryManager.m
//  iCarStyle
//
//  Created by liunian on 13-9-5.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import "CSCoverStoryManager.h"

@interface CSCoverStoryManager ()<ReqDelegate>{
    MKEnvObserverCenter    *_obCenter;
}
@property (nonatomic, strong) ADRequest *reqAD;

@end

static CSCoverStoryManager * sharedManager;
@implementation CSCoverStoryManager

+ (CSCoverStoryManager *)sharedManager{
    if (!sharedManager) {
        sharedManager = [[CSCoverStoryManager alloc] init];
    }
    return sharedManager;
}

- (id)init
{
    self = [super init];
    if (self) {
    }
    return self;
}
- (void)addObserver:(id<CSCoverStoryManagerDelegate>)observer
{
    [_obCenter addEnvObserver:observer];
}

- (void)removeObserver:(id<CSCoverStoryManagerDelegate>)observer
{
    [_obCenter removeEnvObserver:observer];
}


- (void)requestAD{
    [self.reqAD requestADWithDelegate:self];
}

#pragma mark ReqDelegate
- (void)finishErrorReq:(id)Req errorCode:(int) errorCode message:(NSString *)message{
    [_obCenter noticeObervers:@selector(mkManagerAllAdsRequestDidFinishedWithAds:)
                   withObject:nil];
}
//AD
- (void)finishADReq:(id)Req ads:(NSArray *)ads{
    self.ads = ads;
    [_obCenter noticeObervers:@selector(mkManagerAllAdsRequestDidFinishedWithAds:)
                   withObject:ads];
}

#pragma mark getter
- (ADRequest *)reqAD{
    if (!_reqAD) {
        _reqAD = [[ADRequest alloc] init];
    }
    return _reqAD;
}
@end
