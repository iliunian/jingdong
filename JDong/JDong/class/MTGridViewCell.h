//
//  MTGridViewCell.h
//  iMiniTao
//
//  Created by liu nian on 13-8-15.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import "GMGridViewCell.h"
#import "GMGridViewCell+Extended.h"
#import "MeSource.h"

@interface MTGridViewCell : GMGridViewCell
@property (nonatomic, retain) UIImageView    *contentImageView; //频道内容图片
@property (nonatomic, retain) UIImageView    *bottomImageView;  //频道底部背景
@property (nonatomic, retain) MeSource       *source;
@property (nonatomic, retain) UILabel        *nameLabel;        //频道名
@end
