//
//  MKImageView.h
//  iCarStyle
//
//  Created by liunian on 13-9-5.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import <UIKit/UIKit.h>
@class AdItem;
@interface MKImageView : UIImageView
@property (nonatomic, retain) AdItem *adItem;
@end
