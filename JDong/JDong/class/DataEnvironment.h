//
//  DataEnvironment.h
//  Mooker
//
//  Created by Kevin Huang on 9/15/10.
//  Copyright 2010 iTotem. All rights reserved.
//
#import "MDSlideNavigationViewController.h"

@class MainViewController;
@interface DataEnvironment : NSObject {

}

+ (DataEnvironment *)sharedDataEnvironment;

@property(nonatomic,retain) MainViewController *mainViewController;
@property(nonatomic,retain) MDSlideNavigationViewController *navController;

@property(nonatomic,assign) NSInteger numOfDetailView;
@property(nonatomic,assign) BOOL  isMainViewPresentingController;

- (void)initData;
- (UIViewController *)presentingController;
@end


