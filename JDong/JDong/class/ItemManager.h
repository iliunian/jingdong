//
//  ItemManager.h
//  JDong
//
//  Created by liunian on 13-12-16.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MKEnvObserverCenter.h"

@protocol ItemManagerDelegate <NSObject>
@optional

- (void)mkManagerShortUrlDidFinishedWithShortUrl:(NSString *)shortUrl;
- (void)mkManagerShortUrlDidFailed;

@end
@interface ItemManager : NSObject
@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;

+ (ItemManager *)sharedManager;

- (void)addObserver:(id<ItemManagerDelegate>)observer;
- (void)removeObserver:(id<ItemManagerDelegate>)observer;

- (void)itemShortUrlWithItemid:(NSInteger)itemid;
- (void)setShortUrl:(NSString *)shortUrl WithItemid:(NSInteger)itemid;
@end
