//
//  CustomKeyCell.h
//  iMiniTao
//
//  Created by liu nian on 13-8-22.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import "RMSwipeTableViewCell.h"
@protocol CustomKeyCellDelegate;

@class Source;
@class MeSource;
@interface CustomKeyCell : RMSwipeTableViewCell
typedef void (^deleteBlock)(CustomKeyCell *swipeTableViewCell);

@property (nonatomic, strong) UIButton *deleteButton;
@property (nonatomic, strong) deleteBlock deleteBlockHandler;
@property (nonatomic, assign) id <CustomKeyCellDelegate> demoDelegate;


@property (nonatomic, retain) UIImageView    *srcImageView;
@property (nonatomic, retain) UILabel        *nameLabel;
@property (nonatomic, retain) UILabel        *summaryLabel;
@property (nonatomic, retain) UIImageView    *subscribeImageView;
@property (nonatomic, retain) MeSource      *meSource;


-(void)resetContentView;
@end
@protocol CustomKeyCellDelegate <NSObject>
@optional
-(void)swipeTableViewCellDidDelete:(CustomKeyCell*)swipeTableViewCell;
-(void)handleSubscribeForCustomKeyCell:(CustomKeyCell *)cell;
-(void)enterChannelForCustomKeyCell:(CustomKeyCell *)cell;
@end
