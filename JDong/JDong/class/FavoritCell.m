//
//  FavoritCell.m
//  iMiniTao
//
//  Created by liunian on 13-8-19.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import "FavoritCell.h"
#import <QuartzCore/QuartzCore.h>

//频道背景颜色
#define COLOR_RGBA_SOURCE_BG  COLOR_RGBA(0, 174.0f, 238.0f, 0.6f)
#define COLOR_RGB_SOURCE_BG   COLOR_RGB(0, 174.0f, 238.0f)
@implementation FavoritCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor = COLOR_RGBA_SOURCE_BG;
        UIImage *image = IMGNAMED(@"btn_ch_delete.png");
        self.deleteButtonIcon = image;
        self.deleteButtonOffset = CGPointMake(frame.size.width-image.size.width+4, -3);
        
        [self.contentView addSubview:self.contentImageView];
        self.editEnabled = YES;
    }
    return self;
}


- (void)prepareForReuse
{
    [super prepareForReuse];
    [self.contentImageView setImage:nil];
}

#pragma mark - getter
- (UILabel *)nameLabel{
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, self.contentView.bounds.size.width-5, 30)];
        _nameLabel.backgroundColor = [UIColor clearColor];
        _nameLabel.autoresizingMask = UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleBottomMargin;
        _nameLabel.textColor = COLOR_RGB(255.0f, 255.0f, 255.0f);
        _nameLabel.font =  FONT_CONTENT(14.0f);
        _nameLabel.textAlignment = [Util getAlign:ALIGNTYPE_LEFT];
        [self.contentView addSubview:_nameLabel];
    }
    return _nameLabel;
}

- (UIImageView *)contentImageView{
    if (!_contentImageView) {
        _contentImageView = [[UIImageView alloc] initWithFrame:self.bounds];
        _contentImageView.backgroundColor = [UIColor clearColor];
        _contentImageView.contentMode = UIViewContentModeScaleAspectFill;
        _contentImageView.userInteractionEnabled = YES;
        _contentImageView.clipsToBounds = YES;
        
    }
    return _contentImageView;
}

- (UIImageView *)bottomImageView{
    if (!_bottomImageView) {
        UIImage *image = IMGFROMBUNDLE(@"bg_news_count.png");
        image = [image stretchableImageWithLeftCapWidth:floor(image.size.width/2) topCapHeight:floor(image.size.height/2)];
        _bottomImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, image.size.width, image.size.height)];
        _bottomImageView.backgroundColor = [UIColor clearColor];
        //        _bottomImageView.image = image;
        [self.contentView addSubview:_bottomImageView];
    }
    return _bottomImageView;
}
@end
