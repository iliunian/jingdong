//
//  Comment.m
//  JDong
//
//  Created by liunian on 13-12-16.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import "Comment.h"

@implementation Comment
+ (id)createWithDict:(NSDictionary *)dict
{
    return [[Comment alloc] initWithDict:dict];
}

- (id)initWithDict:(NSDictionary *)dict
{
    self = [super init];
    if (self) {
        if (dict) {
            self.id = BUGetObjFromDict(@"id", dict, [NSString class]);
            self.userid = BUGetObjFromDict(@"userid", dict, [NSString class]);
            self.name = BUGetObjFromDict(@"name", dict, [NSString class]);
            self.img = BUGetObjFromDict(@"img", dict, [NSString class]);
            self.content = BUGetObjFromDict(@"content", dict, [NSString class]);
            self.time = BUGetObjFromDict(@"time", dict, [NSString class]);
        }
    }
    return self;
}

#pragma mark - NSCoding
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if (self) {
        self.id = [aDecoder decodeObjectForKey:@"id"];
        self.userid = [aDecoder decodeObjectForKey:@"userid"];
        self.name = [aDecoder decodeObjectForKey:@"name"];
        self.img = [aDecoder decodeObjectForKey:@"img"];
        self.content = [aDecoder decodeObjectForKey:@"content"];
        self.time = [aDecoder decodeObjectForKey:@"time"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.id forKey:@"id"];
    [aCoder encodeObject:self.userid forKey:@"userid"];
    [aCoder encodeObject:self.name forKey:@"name"];
    [aCoder encodeObject:self.img forKey:@"img"];
    [aCoder encodeObject:self.content forKey:@"content"];
    [aCoder encodeObject:self.time forKey:@"time"];
}
@end
