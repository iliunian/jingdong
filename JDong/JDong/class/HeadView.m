//
//  HeadView.m
//  suitDress
//
//  Created by liunian on 13-10-16.
//  Copyright (c) 2013年 liu nian. All rights reserved.
//

#import "HeadView.h"

@implementation HeadView

- (id)initWithFrame:(CGRect)frame withItem:(CuzyTBKItem *)item
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self initWithItem:item];
    }
    return self;
}

- (void)initWithItem:(CuzyTBKItem *)item{
    NSArray *pictures = item.picturesArray;
    if (pictures == nil) {
        pictures = [NSArray arrayWithObject:item.itemImageURLString];
    }
    for (int i = 0; i < [item.picturesArray count]; i++) {
        CGRect frame = CGRectMake(i*CGRectGetWidth(self.scrollView.frame),
                                  0,
                                  CGRectGetWidth(self.scrollView.frame),
                                  CGRectGetHeight(self.scrollView.frame));
        
        UIImageView  *picture = [[UIImageView alloc] initWithFrame:frame];
        [picture setContentMode:UIViewContentModeScaleAspectFill];
        [picture setClipsToBounds:YES];
        [picture setBackgroundColor:[UIColor randomFlatColor]];
        NSString *imageURLStr = [item.picturesArray objectAtIndex:i];
        imageURLStr = [imageURLStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *imageURL = [NSURL URLWithString:item.itemImageURLString];
        [picture setImageWithURL:imageURL placeholderImage:IMGNAMED(@"")];
        [self.scrollView addSubview:picture];
    }
    [self.scrollView setContentSize:CGSizeMake(item.picturesArray.count * CGRectGetWidth(self.scrollView.frame) , CGRectGetHeight(self.scrollView.frame))];
    if (item.picturesArray.count > 1) {
        [self.pageControl setNumberOfPages:item.picturesArray.count];
        [self.pageControl setCurrentPage:0];
    }
    
}

- (void)scrollRectToVisibleByPage:(int)page{
    self.currentPage = page;
}
- (void)pageValueChange:(id)sender{
    UIPageControl  *contol = (UIPageControl *)sender;
    [self scrollRectToVisibleByPage:contol.currentPage];
}
#pragma mark - UIScrollViewDelegate methods
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    NSInteger pageIndex = scrollView.contentOffset.x/CGRectGetWidth(scrollView.frame);
    [self scrollRectToVisibleByPage:pageIndex];
}

- (void)setCurrentPage:(NSInteger)currentPage{
    [self.pageControl setCurrentPage:currentPage];
}
#pragma mark getter
- (UIScrollView *)scrollView{
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 320, CGRectGetHeight(self.bounds))];
        _scrollView.delegate = self;
        _scrollView.bounces = NO;
        _scrollView.pagingEnabled = YES;
        _scrollView.backgroundColor = [UIColor clearColor];
        _scrollView.showsHorizontalScrollIndicator = NO;
        _scrollView.showsVerticalScrollIndicator = NO;
        [self addSubview:_scrollView];
    }
    return _scrollView;
}
- (MKPageControl *)pageControl{
    if (!_pageControl) {
        _pageControl = [[MKPageControl alloc] initWithFrame:CGRectMake(140, self.bounds.size.height-20, 40, 20)];
        _pageControl.backgroundColor = [UIColor clearColor];
        _pageControl.normalImage = [UIImage imageNamed:@"slider_indicator_white.png"];
        _pageControl.highlightedImage = [UIImage imageNamed:@"slider_indicator_highlighted.png"];
        [_pageControl addTarget:self
                         action:@selector(pageValueChange:) forControlEvents:UIControlEventValueChanged];
        [self addSubview:_pageControl];
    }
    return _pageControl;
}
@end
