//
//  CSCoverStoryManager.h
//  iCarStyle
//
//  Created by liunian on 13-9-5.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ADRequest.h"
#import "MKEnvObserverCenter.h"

@protocol CSCoverStoryManagerDelegate <NSObject>
@optional

- (void)mkManagerAllAdsRequestDidFinishedWithAds:(NSArray *)ads;
- (void)mkManagerAllAdsRequestDidFailed;

@end

@interface CSCoverStoryManager : NSObject

- (void)addObserver:(id<CSCoverStoryManagerDelegate>)observer;
- (void)removeObserver:(id<CSCoverStoryManagerDelegate>)observer;

@property (nonatomic, retain) NSArray *ads;

+ (CSCoverStoryManager *)sharedManager;
- (void)requestAD;
@end
