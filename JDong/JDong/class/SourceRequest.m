//
//  SourceRequest.m
//  iMiniTao
//
//  Created by liu nian on 13-8-14.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import "SourceRequest.h"
#import "HttpRequest.h"

#define apiSource @"http://imitao.sinaapp.com/api/source_jd/list/"
@interface SourceRequest ()
@property (nonatomic , assign) id<ReqDelegate>delegate;
@end

@implementation SourceRequest

- (void)requestAllSourcesWithDelegate:(id<ReqDelegate>)delegate;{
    _delegate = nil;
    _delegate = delegate;
    
    NSMutableDictionary *param = [[NSMutableDictionary alloc] initWithCapacity:0];
    SET_PARAM(@"50", @"count", param);
    SET_PARAM(@"asc", @"ord", param);
    SET_PARAM(@"index", @"by", param);
    [HttpRequest requestWithURLStr:apiSource
                             param:param
                        httpMethod:HttpMethodGet
                            isAsyn:YES
                   completionBlock:^(ASIHTTPRequest *request) {
                       NSDictionary *jsonDict = [request.responseString JSONValue];
                       
                       if ([HttpRequest isRequestSuccess:request]) {
                           NSMutableArray *rstArray = [NSMutableArray arrayWithCapacity:0];
                           NSArray *sources = [[jsonDict objectForKey:@"data"] objectForKey:@"items"];
                           if (isArrayWithCountMoreThan0(sources)) {
                               for (NSDictionary *dic in sources) {
                                   if (isDictWithCountMoreThan0(dic)) {
                                        Source *s = [Source createWithDict:dic];
                                       if (s.itemtype !=ITEMTYPENONE) {
                                           [rstArray addObject:s];
                                       }
                                   }
                               }
                               
                               if ([_delegate respondsToSelector:@selector(finishSourceReq:sources:)]) {
                                   [_delegate finishSourceReq:self sources:rstArray];
                               }
                               
                               
                           }else{
                               if ([_delegate respondsToSelector:@selector(finishErrorReq:errorCode:message:)]) {
                                   [_delegate finishErrorReq:self errorCode:1 message:@"无数据"];
                               }
                           }
                       }
    } failedBlock:^(ASIHTTPRequest *request) {
        if ([_delegate respondsToSelector:@selector(finishErrorReq:errorCode:message:)]) {
            [_delegate finishErrorReq:self errorCode:1 message:@"无数据"];
        }
    }];
}
@end
