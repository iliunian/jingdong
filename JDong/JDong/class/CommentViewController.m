//
//  CommentViewController.m
//  JDong
//
//  Created by liunian on 13-12-16.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import "CommentViewController.h"
#import "UIPlaceHolderTextView.h"
#import "SNSManager.h"
#import "MBProgressHUD.h"

#define kDefaultBottomBarHeight           38

@interface CommentViewController ()<UITextViewDelegate,SNSManagerDelegate>
@property (nonatomic, retain) UIImageView          *navImageView;
@property (nonatomic, retain) UIView        *containerView;
@property (nonatomic, retain) UILabel       *theTitle;
@property (nonatomic, retain) UIPlaceHolderTextView *textView;
@property (nonatomic, retain) UITapGestureRecognizer *tapRecognizer;
@property (nonatomic, retain) NSString *weiboID;
@property (nonatomic, retain) MBProgressHUD *progressHUD;
@end

@implementation CommentViewController

- (id)initWithWeiboID:(NSString *)weiboID
{
    self = [super init];
    if (self) {
        // Custom initialization
        self.weiboID = weiboID;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
     self.view.backgroundColor = [UIColor blackColor];
#if __IPHONE_OS_VERSION_MAX_ALLOWED < 70000
    
#else
    float systemName = [[[UIDevice currentDevice] systemVersion] floatValue];
    if(systemName >= 7.0)
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.modalPresentationCapturesStatusBarAppearance = NO;
        [self.navigationController.navigationBar setBarTintColor:COLOR_BG_VIEW];
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleBlackTranslucent animated:YES];
    }
#endif
    
   
    [self.view addSubview:self.navImageView];
    [self.theTitle setText:@"评论"];
    
    [self textView];
    [self tapRecognizer];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - UIButton method
- (void)handleTap:(UITapGestureRecognizer *)tapGesture{
    [self.textView resignFirstResponder];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if ([[SNSManager sharedManager] isLoggedInFor:kOFTypeSina]) {
        
    }else if ([[SNSManager sharedManager] isLoggedInFor:kOFTypeSina]){
        
    }
}

- (void)back{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [self.navigationController popViewControllerAnimated:YES];
//    [self  dismissViewControllerAnimated:YES completion:nil];
}

- (void)send{

    if ([[SNSManager sharedManager] isLoggedInFor:kOFTypeSina]) {
        
        if (self.textView.text == 0) {
            [Util showTipsView:@"请输入评论内容"];
            return;
        }
        
        [[SNSManager sharedManager] sendCommentWithText:self.textView.text weiboID:self.weiboID delegate:self forType:kOFTypeSina];
        [self.progressHUD startAnimating];
    }else{
        [[SNSManager sharedManager] loginFor:kOFTypeSina withDelegate:self];
    }
    
}

#pragma mark SNSManagerDelegate
- (void)snsManagerLoginDidSuccessFor:(OFTypeE)type{
    
}
- (void)snsManagerLoginDidFailFor:(OFTypeE)type{

}

- (void)snsManagerAuthorizeExpired{

}

//send comment
- (void)snsManagerSendCommentDidSuccessFor:(OFTypeE)type weiboId:(NSString *)weiboId{
    if (self.progressHUD.isStarting) {
        [self.progressHUD stopAnimating];
    }
    [self back];
}
- (void)snsManagerSendCommentDidFailFor:(OFTypeE)type withError:(SNSManagerErrorTypeE)error{
    if (self.progressHUD.isStarting) {
        [self.progressHUD stopAnimating];
    }
}
#pragma mark UITextViewDelegate
- (void)textViewDidChange:(UITextView *)textView{
    NSInteger number = [textView.text length];
    if (number > 140) {
        [Util showTipsView:@"字数不可超过140个"];
        textView.text = [textView.text substringToIndex:140];
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if([text isEqualToString:@"\n"]) {
        [self send];
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView{
    [UIView beginAnimations:nil context:NULL];
    CGAffineTransform moveTransform = CGAffineTransformMakeTranslation(0.0, -0.0);
    [self.view.layer setAffineTransform:moveTransform];
    self.view.opaque =1;
    [UIView commitAnimations];
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    [UIView beginAnimations:nil context:NULL];
    CGAffineTransform moveTransform = CGAffineTransformMakeTranslation(0.0, 0.0);
    [self.view.layer setAffineTransform:moveTransform];
    self.view.opaque =1;
    [UIView commitAnimations];
}

#pragma mark - getter
- (UIImageView *)navImageView{
    if (!_navImageView) {
        _navImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, kDefaultBottomBarHeight)];
        _navImageView.userInteractionEnabled = YES;
        
        _navImageView.backgroundColor = COLOR_BG_VIEW;
        [_navImageView setImage:IMGNAMED(@"categorySearchBarBackground.png")];
        UIImage *imgBack = IMGNAMED(@"btn_back.png");
        
        UIButton *back = [UIButton buttonWithType:UIButtonTypeCustom];
        back.exclusiveTouch = YES;
        back.frame = CGRectMake(10, floorf((_navImageView.bounds.size.height-imgBack.size.height)/2), imgBack.size.width, imgBack.size.height);
        [back addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
        [_navImageView addSubview:back];
        
        UIImage *imgOK = IMGNAMED(@"btn_newok.png");
        
        UIButton *sendBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        sendBtn.exclusiveTouch = YES;
        sendBtn.frame = CGRectMake(280, floorf((_navImageView.bounds.size.height-imgOK.size.height)/2), imgOK.size.width, imgOK.size.height);
        [sendBtn addTarget:self action:@selector(send) forControlEvents:UIControlEventTouchUpInside];
        [_navImageView addSubview:sendBtn];
        
        [back setBackgroundImage:IMGNAMED(@"btn_back.png") forState:UIControlStateNormal];
        [back setBackgroundImage:IMGNAMED(@"btn_back_light.png") forState:UIControlStateHighlighted];
        
        [sendBtn setBackgroundImage:IMGNAMED(@"btn_newok.png") forState:UIControlStateNormal];
        [sendBtn setBackgroundImage:IMGNAMED(@"btn_newok_light.png") forState:UIControlStateHighlighted];
        
    }
    return _navImageView;
}

- (UILabel *)theTitle{
    if (!_theTitle) {
        _theTitle = [[UILabel alloc] initWithFrame:CGRectMake(50, 4, 160, 40)];
        _theTitle.backgroundColor = [UIColor clearColor];
        _theTitle.textAlignment = [Util getAlign:ALIGNTYPE_LEFT];
        _theTitle.font = FONT_TITLE(18.0f);
        _theTitle.textColor = [UIColor flatDarkBlackColor];
        [self.navImageView addSubview:_theTitle];
    }
    return _theTitle;
}
- (UIView *)containerView{
    if (!_containerView) {
        _containerView = [[UIView alloc] initWithFrame:CGRectMake(0,
                                                                  CGRectGetMaxY(self.navImageView.frame),
                                                                  CGRectGetWidth(self.view.bounds),
                                                                  CGRectGetHeight(self.view.bounds) - CGRectGetHeight(self.navImageView.frame)-_kSpaceHeight)];
        [_containerView setBackgroundColor:COLOR_BG_VIEW];
        [self.view addSubview:_containerView];
    }
    return _containerView;
}

- (UITextView *)textView{
    if (!_textView) {
        _textView = [[UIPlaceHolderTextView alloc] initWithFrame:CGRectMake(20, CGRectGetMaxY(self.navImageView.frame) + 10, 280, 120)];
        [_textView setBackgroundColor:[UIColor whiteColor]];
        [_textView setReturnKeyType:UIReturnKeySend];
        [_textView setPlaceholder:@"请输入评论内容"];
        [_textView setDelegate:self];
        [_textView setKeyboardType:UIKeyboardTypeDefault];
        [_textView setTextColor:[UIColor darkGrayColor]];
        [_textView setFont:[UIFont fontWithName:@"Verdana" size:16]];
        [self.containerView addSubview:_textView];
    }
    return _textView;
}


- (UITapGestureRecognizer *)tapRecognizer{
    if (!_tapRecognizer) {
        _tapRecognizer= [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
        [_tapRecognizer setNumberOfTapsRequired:1];
//        _tapRecognizer.delegate = self;
        [self.view addGestureRecognizer:_tapRecognizer];
    }
    return _tapRecognizer;
}

- (MBProgressHUD *)progressHUD
{
    if (!_progressHUD) {
        _progressHUD = [[MBProgressHUD alloc] initWithView:self.view];
        _progressHUD.labelFont = FONT_CONTENT(18.0);
        _progressHUD.labelText = @"正在发送...";
        [self.view addSubview:_progressHUD];
    }
    return _progressHUD;
}
@end
