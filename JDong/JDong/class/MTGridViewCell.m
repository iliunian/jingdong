//
//  MTGridViewCell.m
//  iMiniTao
//
//  Created by liu nian on 13-8-15.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import "MTGridViewCell.h"
#import <QuartzCore/QuartzCore.h>
#import "RandomMetro.h"

//频道背景颜色
#define COLOR_RGBA_SOURCE_BG  COLOR_RGBA(0, 174.0f, 238.0f, 0.6f)
#define COLOR_RGB_SOURCE_BG   COLOR_RGB(0, 174.0f, 238.0f)


#define COLOR1  
@interface MTGridViewCell ()

@end

@implementation MTGridViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor = COLOR_RGBA_SOURCE_BG;
        UIImage *image = IMGNAMED(@"btn_ch_delete.png");
        self.deleteButtonIcon = image;
        self.deleteButtonOffset = CGPointMake(frame.size.width-image.size.width+4, -3);
        [self.contentView addSubview:self.contentImageView];
    }
    return self;
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    [self.contentImageView setImage:nil];
}

- (void)showContent{

    CGFloat width = ceil(100+8);
    CGRect frame = self.bottomImageView.frame;
    frame.size.width = width>frame.size.height?width:frame.size.height;
    frame.origin = CGPointMake(self.contentView.bounds.size.width-frame.size.width-5,
                               self.contentView.bounds.size.height-frame.size.height-5+3);
    self.bottomImageView.frame = frame;
}

- (void)userStatusChanged{

    CGRect frame = self.bottomImageView.frame;
    
    frame.size.width = self.contentView.bounds.size.width-10;
    frame.origin = CGPointMake(5,
                               self.contentView.bounds.size.height-frame.size.height-5+3);
    self.bottomImageView.frame = frame;
}

#pragma mark - setter
- (void)setSource:(MeSource *)source{
    _source = source;
    self.backgroundColor = COLOR_RGBA_SOURCE_BG;
    
    if (_source == nil) {
        
        if (_bottomImageView) {
            [_bottomImageView removeFromSuperview];
            RELEASE_SAFELY(_bottomImageView);
        }
        
        self.nameLabel.text = @"";
        NSString  *imageUrl = @"";
        [self.contentImageView setImageWithURL:[NSURL URLWithString:imageUrl] placeholderImage:IMGFROMBUNDLE(@"bg_add_channel.png")];
    }else{
        self.nameLabel.text = self.source.name;
        
        if (source.custom) {
            NSInteger index = arc4random() % 23;
            UIImage *randomImage = [UIImage imageNamed:[NSString stringWithFormat:@"ca_custom%d.png",index]];
            [self.nameLabel setFrame:CGRectMake(5, self.contentView.bounds.size.height/4, self.contentView.bounds.size.width-10, self.contentView.bounds.size.height/2)];
            [self.nameLabel setFont:FONT_TITLE(18.0f)];
            self.nameLabel.textAlignment = [Util getAlign:ALIGNTYPE_CENTER];
            [self showContent];
            [self.contentImageView setImageWithURL:[NSURL URLWithString:nil] placeholderImage:randomImage];
        }else{
            [self.nameLabel setFrame:CGRectMake(5, 0, self.contentView.bounds.size.width-5, 30)];
            [self.nameLabel setFont:FONT_CONTENT(14.0f)];
            self.nameLabel.textAlignment = [Util getAlign:ALIGNTYPE_LEFT];
            [self showContent];
            [self.contentImageView setImageWithURL:[NSURL URLWithString:self.source.icon] placeholderImage:self.contentImageView.image];
        }
    }
    if (_source == nil) {
        self.editEnabled = NO;
    }else{
        self.editEnabled = YES;
    }
    
    
}

#pragma mark - getter
- (UILabel *)nameLabel{
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, self.contentView.bounds.size.width-5, 30)];
        _nameLabel.backgroundColor = [UIColor clearColor];
        _nameLabel.autoresizingMask = UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleBottomMargin;
        _nameLabel.textColor = [UIColor flatWhiteColor];
        _nameLabel.font =  [UIFont fontWithName:@"HelveticaNeue-Light" size:14];
        _nameLabel.textAlignment = [Util getAlign:ALIGNTYPE_LEFT];
        [self.contentView addSubview:_nameLabel];
    }
    return _nameLabel;
}

- (UIImageView *)contentImageView{
    if (!_contentImageView) {
        _contentImageView = [[UIImageView alloc] initWithFrame:self.bounds];
        _contentImageView.backgroundColor = [UIColor clearColor];
        _contentImageView.contentMode = UIViewContentModeScaleAspectFill;
        _contentImageView.userInteractionEnabled = YES;
        _contentImageView.clipsToBounds = YES;
        
    }
    return _contentImageView;
}

- (UIImageView *)bottomImageView{
    if (!_bottomImageView) {
        UIImage *image = IMGFROMBUNDLE(@"bg_news_count.png");
        image = [image stretchableImageWithLeftCapWidth:floor(image.size.width/2) topCapHeight:floor(image.size.height/2)];
        _bottomImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, image.size.width, image.size.height)];
        _bottomImageView.backgroundColor = [UIColor clearColor];
        //        _bottomImageView.image = image;
        [self.contentView addSubview:_bottomImageView];
    }
    return _bottomImageView;
}

@end
