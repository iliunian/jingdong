//
//  CommentRequest.h
//  JDong
//
//  Created by liunian on 13-12-13.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ReqDelegate.h"

@interface CommentRequest : NSObject
@property (nonatomic , assign) id<ReqDelegate>delegate;
- (void)handleWeiboIdWithItemID:(NSString *)itemid
                       itemName:(NSString *)itemname
                          image:(NSString *)image
                            url:(NSString *)url
                          price:(NSString *)price
                            tag:(NSString *)tag
                       delegate:(id<ReqDelegate>)delegate;
@end
