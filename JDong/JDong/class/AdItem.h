//
//  AdItem.h
//  iCarStyle
//
//  Created by liunian on 13-9-5.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef enum {
    MKAdClickTypeUrl = 1,
    MKAdClickTypeApp,
    MKAdClickTypeVideo
} MKAdClickTypeE;

@interface AdItem : NSObject
@property (nonatomic, assign) NSInteger index;
@property (nonatomic, retain) NSString *title;//广告图片
@property (nonatomic, retain) NSString *adUrl;//广告图片
@property (nonatomic, retain) NSString *clickUrl;
@property (nonatomic, assign) MKAdClickTypeE clickType;
+ (id)createWithDict:(NSDictionary *)dict;
- (id)initWithDict:(NSDictionary *)dict;
@end
