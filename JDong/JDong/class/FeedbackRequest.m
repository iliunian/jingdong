//
//  FeedbackRequest.m
//  JDong
//
//  Created by liunian on 13-12-17.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import "FeedbackRequest.h"
#import "HttpRequest.h"

#define kFeedbackAPI @"http://imitao.sinaapp.com/api/feedback/insert/"

@interface FeedbackRequest ()

@end

@implementation FeedbackRequest
- (void)sendContent:(NSString *)content email:(NSString *)email delegate:(id<ReqDelegate>)delegate;{
    _delegate = nil;
    _delegate = delegate;

    NSMutableDictionary *param = [[NSMutableDictionary alloc] initWithCapacity:0];

    SET_PARAM(content, @"content", param);
    SET_PARAM(email, @"email", param);
    
    [HttpRequest requestWithURLStr:kFeedbackAPI
                             param:param
                        httpMethod:HttpMethodGet
                            isAsyn:YES
                   completionBlock:^(ASIHTTPRequest *request) {
                       NSDictionary *jsonDict = [request.responseString JSONValue];
                       if (isDictWithCountMoreThan0(jsonDict)) {
                           
                           if ([_delegate respondsToSelector:@selector(finishFeedback:)]) {
                               [_delegate finishFeedback:self];
                           }
                       }else{
                           if ([_delegate respondsToSelector:@selector(finishErrorReq:errorCode:message:)]) {
                               [_delegate finishErrorReq:self errorCode:1 message:@"无数据"];
                           }
                       }
                       BMLog(@"jsonDict:%@",jsonDict);

                   } failedBlock:^(ASIHTTPRequest *request) {
                       if ([_delegate respondsToSelector:@selector(finishErrorReq:errorCode:message:)]) {
                           [_delegate finishErrorReq:self errorCode:1 message:@"无数据"];
                       }
                   }];
}
@end
