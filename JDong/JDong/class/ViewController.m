//
//  ViewController.m
//  JDong
//
//  Created by liunian on 13-12-10.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import "ViewController.h"
#import "LeoLoadingView.h"
@interface ViewController ()
@property (nonatomic, retain) LeoLoadingView *loadingView;
@end

@implementation ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _kSpaceHeight = 0;
    UIColor *color = [UIColor getColor:@"E33C3E"];
    
    self.view.backgroundColor = color;
#if __IPHONE_OS_VERSION_MAX_ALLOWED < 70000
    
#else
    float systemName = [[[UIDevice currentDevice] systemVersion] floatValue];
    if(systemName >= 7.0)
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.modalPresentationCapturesStatusBarAppearance = NO;
        [self.navigationController.navigationBar setBarTintColor:color];
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
        self.view.bounds = CGRectMake(0, -20, self.view.frame.size.width, self.view.frame.size.height);
        _kSpaceHeight = 20;
    }
#endif
}

#if __IPHONE_OS_VERSION_MAX_ALLOWED < 70000

#else
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
#endif



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)loading:(BOOL)loading{
    [self.loadingView showView:loading];
}
#pragma mark getter
- (LeoLoadingView *)loadingView{
    if (!_loadingView) {
        _loadingView = [[LeoLoadingView alloc] initWithFrame:CGRectMake(120, 200, 40, 40)];
        [self.view addSubview:_loadingView];
    }
    return _loadingView;
}


@end
