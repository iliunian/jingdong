//
//  FqViewController.m
//  iMiniTao
//
//  Created by liunian on 13-8-28.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import "FqViewController.h"
#import "WebViewController.h"

#define kFQWebURL   @"http://iminitao.com/app/fq.html"

#define kDefaultBottomBarHeight           38

@interface FqViewController ()<UIWebViewDelegate>
@property (nonatomic, retain) UIImageView          *navImageView;
@property (nonatomic, retain) UIImageView          *bottomImageView;
@property (nonatomic, retain) UILabel       *theTitle;
@property (nonatomic, retain) UIWebView     *webView;
@property (nonatomic, retain) UIActivityIndicatorView *indicatorView;
@end

@implementation FqViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.view addSubview:self.navImageView];
    [self.view addSubview:self.bottomImageView];
    [self.theTitle setText:@"帮助"];
    NSURLRequest* request = [NSURLRequest requestWithURL:[NSURL URLWithString:kFQWebURL]];
    if (request) {
        [self.indicatorView startAnimating];
        [self.webView loadRequest:request];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)back{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark UIWebViewDelegate
- (void)webViewDidStartLoad:(UIWebView *)webView {
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    [self.indicatorView startAnimating];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [self.indicatorView stopAnimating];
}
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    if (navigationType == UIWebViewNavigationTypeLinkClicked) {
        WebViewController *webVC = [[WebViewController alloc] initWithURL:request.URL];
        [self.navigationController pushViewController:webVC animated:YES];
        return NO;
    }
    return YES;
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [self.indicatorView stopAnimating];
}
#pragma mark - getter
- (UIImageView *)navImageView{
    if (!_navImageView) {
        _navImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, kDefaultBottomBarHeight)];
        _navImageView.userInteractionEnabled = YES;
        _navImageView.backgroundColor = COLOR_RGB_NAV;
        [self.indicatorView setFrame:CGRectMake(CGRectGetWidth(_navImageView.frame) - 36 - 10, 5, 36, 36)];
        [_navImageView addSubview:self.indicatorView];
        
    }
    return _navImageView;
}
- (UIActivityIndicatorView *)indicatorView{
    if (!_indicatorView) {
        _indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        _indicatorView.hidesWhenStopped = YES;
    }
    return _indicatorView;
}
- (UILabel *)theTitle{
    if (!_theTitle) {
        _theTitle = [[UILabel alloc] initWithFrame:CGRectMake(10, 4, 260, 40)];
        _theTitle.backgroundColor = [UIColor clearColor];
        _theTitle.textAlignment = [Util getAlign:ALIGNTYPE_LEFT];
        _theTitle.font = FONT_TITLE(18.0f);
        _theTitle.textColor = [UIColor flatWhiteColor];
        [self.navImageView addSubview:_theTitle];
    }
    return _theTitle;
}
- (UIImageView *)bottomImageView{
    if (!_bottomImageView) {
        _bottomImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height - kDefaultBottomBarHeight -_kSpaceHeight, 320, kDefaultBottomBarHeight)];
        _bottomImageView.backgroundColor = COLOR_RGB_BOTTOM;
        _bottomImageView.userInteractionEnabled = YES;
        [_bottomImageView setImage:IMGNAMED(@"bottomTabBarBackground.png")];
        
        UIImage *imgBack = IMGNAMED(@"btn_back.png");
        UIButton *back = [UIButton buttonWithType:UIButtonTypeCustom];
        back.exclusiveTouch = YES;
        back.frame = CGRectMake(10, floorf((_bottomImageView.bounds.size.height-imgBack.size.height)/2), imgBack.size.width, imgBack.size.height);
        [back addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
        [_bottomImageView addSubview:back];

        [back setBackgroundImage:IMGNAMED(@"btn_back.png") forState:UIControlStateNormal];
        [back setBackgroundImage:IMGNAMED(@"btn_back_light.png") forState:UIControlStateHighlighted];
    }
    return _bottomImageView;
}

- (UIWebView *)webView{
    if (!_webView) {
        _webView = [[UIWebView alloc] initWithFrame:CGRectMake(0,
                                                               self.navImageView.frame.origin.y+self.navImageView.bounds.size.height,
                                                               320,
                                                               self.view.bounds.size.height - kDefaultBottomBarHeight-self.navImageView.frame.origin.y-self.navImageView.bounds.size.height - _kSpaceHeight)];
        _webView.scalesPageToFit = NO;
        _webView.delegate = self;
        _webView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        _webView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
        [self.view addSubview:_webView];
    }
    return _webView;
}
@end
