//
//  CommentCell.h
//  JDong
//
//  Created by liunian on 13-12-16.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Comment;
@interface CommentCell : UITableViewCell
@property (nonatomic, retain) UIImageView    *head;
@property (nonatomic, retain) UILabel        *nameLabel;
@property (nonatomic, retain) UILabel        *contentLabel;
@property (nonatomic, retain) UIImageView    *line;
- (void)updateComment:(Comment *)comment;
+ (CGFloat)heightWithComment:(Comment *)comment;
@end
