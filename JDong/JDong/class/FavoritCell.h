//
//  FavoritCell.h
//  iMiniTao
//
//  Created by liunian on 13-8-19.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import "GMGridViewCell.h"
#import "GMGridViewCell+Extended.h"

@interface FavoritCell : GMGridViewCell
@property (nonatomic, retain) UIImageView    *contentImageView; //频道内容图片
@property (nonatomic, retain) UIImageView    *bottomImageView;  //频道底部背景
@property (nonatomic, retain) UILabel        *nameLabel;        //频道名
@end
