//
//  CoverStory.h
//  iCarStyle
//
//  Created by liunian on 13-9-5.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    MKCoverStoryNotCached,
    MKCoverStoryCaching,
    MKCoverStoryCached
}MKCoverStoryCacheStatusE;

@class AdItem;
@interface CoverStory : NSObject{
    MKCoverStoryCacheStatusE _cacheStatus;
    BOOL _showed;
}
@property (nonatomic, retain) NSString  *storyId;
@property (nonatomic, strong) NSString *imageUrl;
@property (nonatomic, retain) AdItem *adItem;
@property (nonatomic, assign) BOOL showed;

- (void)cacheImage;
- (BOOL)isCached;
@end
