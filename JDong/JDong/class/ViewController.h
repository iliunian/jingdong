//
//  ViewController.h
//  JDong
//
//  Created by liunian on 13-12-10.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController{
    CGFloat     _kSpaceHeight ;
}

- (void)loading:(BOOL)loading;
@end
