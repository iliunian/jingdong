//
//  SourceRequest.h
//  iMiniTao
//
//  Created by liu nian on 13-8-14.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ReqDelegate.h"
#import "Source.h"

@interface SourceRequest : NSObject

- (void)requestAllSourcesWithDelegate:(id<ReqDelegate>)delegate;
@end
