//
//  MainViewController.h
//  iMiniTao
//
//  Created by liunian on 13-8-14.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface MainViewController : UIViewController
@property (nonatomic, assign) BOOL slideEnabled;
@property (nonatomic, assign) BOOL mainViewShown;
- (void)hideCover;
@end
