//
//  CommentViewController.h
//  JDong
//
//  Created by liunian on 13-12-16.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"
@interface CommentViewController : ViewController
- (id)initWithWeiboID:(NSString *)weiboID;
@end
