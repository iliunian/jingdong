//
//  Comment.h
//  JDong
//
//  Created by liunian on 13-12-16.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Comment : NSObject
@property (nonatomic, retain) NSString  *id;
@property (nonatomic, retain) NSString  *userid;
@property (nonatomic, retain) NSString  *name;
@property (nonatomic, retain) NSString  *img;
@property (nonatomic, retain) NSString  *content;
@property (nonatomic, retain) NSString  *time;
+ (id)createWithDict:(NSDictionary *)dict;
- (id)initWithDict:(NSDictionary *)dict;
@end
