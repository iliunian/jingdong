//
//  HeadView.h
//  suitDress
//
//  Created by liunian on 13-10-16.
//  Copyright (c) 2013年 liu nian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MKPageControl.h"
@interface HeadView : UIView<UIScrollViewDelegate>{
    NSInteger  _currentPage;
}
@property (nonatomic, assign) NSInteger  currentPage;
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) MKPageControl *pageControl;

- (id)initWithFrame:(CGRect)frame withItem:(CuzyTBKItem *)item;
@end
