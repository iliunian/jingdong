//
//  CommentCell.m
//  JDong
//
//  Created by liunian on 13-12-16.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import "CommentCell.h"
#import "Comment.h"

@implementation CommentCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)updateComment:(Comment *)comment{
    [self.line setFrame:CGRectMake(10, 0, CGRectGetWidth(self.bounds), 1)];
    [self.head setImageWithURL:[NSURL URLWithString:comment.img] placeholderImage:IMGNAMED(@"bg_weibo_user.png")];
    [self.nameLabel setText:comment.name];
    [self.contentLabel setText:comment.content];
    
    CGSize size = [comment.content sizeWithFont:self.contentLabel.font constrainedToSize:CGSizeMake(230, 1000) lineBreakMode:NSUILineBreakModeWordWrap];
    if (size.height < 20) {
        size.height = 20;
    }
    
    [self.contentLabel setFrame:CGRectMake(70, 30, 230, size.height)];
}

+ (CGFloat)heightWithComment:(Comment *)comment{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(70, 25, 230, 35)];
    label.font =  FONT_CONTENT(14.0f);
    label.numberOfLines = 0;
    label.textAlignment = ALIGNTYPE_LEFT;
    
    CGSize size = [comment.content sizeWithFont:label.font constrainedToSize:CGSizeMake(230, 1000) lineBreakMode:NSUILineBreakModeWordWrap];
    if (size.height < 20) {
        size.height = 20;
    }
    
    return size.height + 40;
}


#pragma mark - getter
- (UIImageView *)line{
    if (!_line) {
        _line = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.bounds), 1)];
        _line.backgroundColor = [UIColor clearColor];
        [_line setImage:IMGNAMED(@"listCellLine.png")];
        _line.contentMode = UIViewContentModeScaleAspectFill;
        _line.userInteractionEnabled = YES;
        _line.clipsToBounds = YES;
        [self.contentView addSubview:_line];
    }
    return _line;
}
- (UIImageView *)head{
    if (!_head) {
        _head  = [[UIImageView alloc] initWithFrame:CGRectMake(10, 5, 50, 50)];
        _head.backgroundColor = [UIColor clearColor];
        _head.contentMode = UIViewContentModeScaleAspectFill;
        _head.userInteractionEnabled = YES;
        _head.clipsToBounds = YES;
        
        [self.contentView addSubview:_head];
    }
    return _head;
}

- (UILabel *)nameLabel{
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(70, 5, 210, 20)];
        _nameLabel.backgroundColor = [UIColor clearColor];
        _nameLabel.textColor = COLOR_RGB(0, 0, 0);
        _nameLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:18];
        [self.contentView addSubview:_nameLabel];
    }
    return _nameLabel;
}

- (UILabel *)contentLabel{
    if (!_contentLabel) {
        _contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(70, 30, 230, 20)];
        _contentLabel.backgroundColor = [UIColor clearColor];
        _contentLabel.textColor = [UIColor darkGrayColor];
        _contentLabel.font = FONT_CONTENT(14);
        [self.contentView addSubview:_contentLabel];
    }
    return _contentLabel;
}

@end
