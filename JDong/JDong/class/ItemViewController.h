//
//  ItemViewController.h
//  iMiniTao
//
//  Created by liunian on 13-8-19.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ItemViewController : UIViewController
@property (nonatomic, retain) UIImage   *shareImage;
@property (nonatomic, retain) NSString  *catalog;
- (id)initWithMeItem:(CuzyTBKItem *)item;
@end
