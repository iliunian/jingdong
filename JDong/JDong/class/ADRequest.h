//
//  ADRequest.h
//  iCarStyle
//
//  Created by liunian on 13-9-6.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ReqDelegate.h"
#import "AdItem.h"

@interface ADRequest : NSObject
- (void)requestADWithDelegate:(id<ReqDelegate>)delegate;
@end
