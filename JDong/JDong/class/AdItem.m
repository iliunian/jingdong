//
//  AdItem.m
//  iCarStyle
//
//  Created by liunian on 13-9-5.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import "AdItem.h"

@implementation AdItem
+ (id)createWithDict:(NSDictionary *)dict
{
    return [[AdItem alloc] initWithDict:dict];
}

- (id)initWithDict:(NSDictionary *)dict
{
    self = [super init];
    if (self) {
        self.index = [BUGetObjFromDict(@"index", dict, [NSString class]) integerValue];
        self.adUrl = BUGetObjFromDict(@"adUrl", dict, [NSString class]);
        self.title = BUGetObjFromDict(@"title", dict, [NSString class]);
        self.clickUrl = BUGetObjFromDict(@"clickUrl", dict, [NSString class]);
        self.clickType = [BUGetObjFromDict(@"clickType", dict, [NSString class]) integerValue];
    }
    return self;
}

@end
