//
//  ListViewController.h
//  iMiniTao
//
//  Created by liu nian on 13-8-17.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import "ViewController.h"

@class MeSource;
@interface ListViewController : ViewController
- (id)initWithMeSource:(MeSource *)source;
@end
