//
//  CoverStory.m
//  iCarStyle
//
//  Created by liunian on 13-9-5.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import "CoverStory.h"
#import "SDImageCache.h"
#import "SDWebImageManager.h"
@implementation CoverStory
+ (id)createWithDict:(NSDictionary *)dict
{
    return [[CoverStory alloc] initWithDict:dict];
}

- (id)initWithDict:(NSDictionary *)dict
{
    self = [super init];
    if (self) {
        self.storyId = BUGetObjFromDict(@"id", dict, [NSString class]);
        self.imageUrl = BUGetObjFromDict(@"image", dict, [NSString class]);
        if ([[SDImageCache sharedImageCache] imageFromMemoryCacheForKey:self.imageUrl]) {
            _cacheStatus = MKCoverStoryCached;
        } else {
            _cacheStatus = MKCoverStoryNotCached;
        }
        _showed = NO;
        
        self.adItem = nil;
    }
    return self;
}


- (BOOL)isCached
{
    if (_cacheStatus == MKCoverStoryCached) {
        return YES;
    } else {
        return NO;
    }
}

- (void)cacheImage
{
    if ((!_imageUrl || [_imageUrl isEqualToString:@""]) ||  // 错误的URL
        _cacheStatus != MKCoverStoryNotCached) {              // 在未缓存状态
        return;
    }
    _cacheStatus = MKCoverStoryCaching;
    [[SDWebImageManager sharedManager] downloadWithURL:[NSURL URLWithString:self.imageUrl] options:SDWebImageLowPriority|SDWebImageRetryFailed
                                              progress:^(NSUInteger receivedSize, long long expectedSize) {
        
    
                                              } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished) {
                                                  if (image) {
                                                      _cacheStatus = MKCoverStoryCached;
                                                  }else{
                                                      _cacheStatus = MKCoverStoryNotCached;
                                                  }
                                                  
                                                  if (error) {
                                                      _cacheStatus = MKCoverStoryNotCached;
                                                  }
                                              }];
}

@end
