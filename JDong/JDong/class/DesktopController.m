//
//  DesktopController.m
//  suitDress
//
//  Created by liunian on 13-10-17.
//  Copyright (c) 2013年 liu nian. All rights reserved.
//

#import "DesktopController.h"
#import "SlideImageView.h"
#import "ApplicationManager.h"



#define kDefaultBottomBarHeight           38

#define BROWSER_TITLE_LBL_TAG 12731
#define BROWSER_DESCRIP_LBL_TAG 178273
#define BROWSER_LIKE_BTN_TAG 12821

@interface DesktopController ()<SlideImageViewDelegate,UIAlertViewDelegate>{
    NSInteger   _clickIndex;
}
@property (nonatomic, retain) UIImageView          *bottomImageView;
@property (nonatomic, retain) SlideImageView *slideImageView;
@end

@implementation DesktopController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setBackgroundColor:COLOR_BG_VIEW];
    _clickIndex = 0;
    [self.view addSubview:self.bottomImageView];
    for(int i=0; i<14; i++)
    {
        NSString* imageName = [NSString stringWithFormat:@"bg_source_%d.jpg",i];
        UIImage* image = [UIImage imageNamed:imageName];
        [self.slideImageView addImage:image];
    }
    [self.slideImageView setImageShadowsWtihDirectionX:2 Y:2 Alpha:0.7];
    [self.slideImageView reLoadUIview];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)back{
    [self dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark SlideImageViewDelegate
- (void)SlideImageViewDidClickWithIndex:(int)index
{
    NSString* indexStr = [[NSString alloc]initWithFormat:@"点击了第%d张",index];
    BMLog(@"%@",indexStr);
    _clickIndex = index;
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"确定设置该图片为桌面背景么?" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    [alert setTag:10013];
    [alert show];
}

- (void)SlideImageViewDidEndScorllWithIndex:(int)index
{
    NSString* indexStr = [[NSString alloc]initWithFormat:@"当前为第%d张",index];
}
#pragma mark UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 0) {
        return;
    }
     NSString* imageName = [NSString stringWithFormat:@"bg_source_%d.jpg",_clickIndex];
    [[ApplicationManager sharedManager] setBackgroundImage:imageName];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"桌面背景设置成功" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];
    [alert show];
}
#pragma mark getter
- (SlideImageView*)slideImageView{
    if (nil == _slideImageView) {
        CGRect rect;
        rect = CGRectMake(20, 40, 250, 340);
        _slideImageView = [[SlideImageView alloc]initWithFrame:rect ZMarginValue:5 XMarginValue:10 AngleValue:0.3 Alpha:1000];
        _slideImageView.borderColor = COLOR_BG_VIEW;
        _slideImageView.delegate = self;
        [self.view addSubview:_slideImageView];
    }
    return _slideImageView;
}

- (UIImageView *)bottomImageView{
    if (!_bottomImageView) {
        _bottomImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height - kDefaultBottomBarHeight, 320, kDefaultBottomBarHeight)];
        _bottomImageView.backgroundColor = COLOR_RGB_BOTTOM;
        _bottomImageView.userInteractionEnabled = YES;
        [_bottomImageView setImage:IMGNAMED(@"bottomTabBarBackground.png")];
        
        UIImage *imgBack = IMGNAMED(@"btn_back.png");
        UIButton *back = [UIButton buttonWithType:UIButtonTypeCustom];
        back.exclusiveTouch = YES;
        back.frame = CGRectMake(10, floorf((_bottomImageView.bounds.size.height-imgBack.size.height)/2), imgBack.size.width, imgBack.size.height);
        [back addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
        [_bottomImageView addSubview:back];
    
        [back setBackgroundImage:IMGNAMED(@"btn_back.png") forState:UIControlStateNormal];
        [back setBackgroundImage:IMGNAMED(@"btn_back_light.png") forState:UIControlStateHighlighted];
    }
    return _bottomImageView;
}
@end
