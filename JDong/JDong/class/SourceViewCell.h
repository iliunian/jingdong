//
//  SourceViewCell.h
//  Mooker
//
//  Created by JK.Peng on 13-1-3.
//
//

#import <UIKit/UIKit.h>
@class Source;
@class SourceViewCell;
@class MeSource;
@protocol SourceViewCellDelegate <NSObject>

@optional
-(void)handleSubscribeForSourceViewCell:(SourceViewCell *)cell;
-(void)enterChannelForSourceViewCell:(SourceViewCell *)cell;
@end

@interface SourceViewCell : UITableViewCell

@property (nonatomic, retain) UIImageView    *srcImageView;
@property (nonatomic, retain) UILabel        *nameLabel;
@property (nonatomic, retain) UILabel        *summaryLabel;
@property (nonatomic, retain) UIImageView    *subscribeImageView;
@property (nonatomic, retain) Source       *source;
@property (nonatomic, assign) id<SourceViewCellDelegate> delegate;

@end
