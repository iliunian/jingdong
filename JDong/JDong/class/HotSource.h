//
//  HotSource.h
//  iMiniTao
//
//  Created by liunian on 13-8-16.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum
{
    HotSourceAD = 0,
    HotSourceItem
} HotSourceType;

@interface HotSource : NSObject
@property (nonatomic, retain) NSString *hid;
@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) NSString *image;
@property (nonatomic, assign) HotSourceType type;
@property (nonatomic, retain) NSString *url;
@property (nonatomic, retain) NSString *itemid;

+ (id)createWithDict:(NSDictionary *)dict;
@end
