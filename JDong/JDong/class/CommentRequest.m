//
//  CommentRequest.m
//  JDong
//
//  Created by liunian on 13-12-13.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import "CommentRequest.h"
#import "HttpRequest.h"
#import "Comment.h"

@interface CommentRequest ()

@end

#define apiWeiboComment     @"http://zhengpinjie.sinaapp.com/weibo.php"
@implementation CommentRequest
- (void)handleWeiboIdWithItemID:(NSString *)itemid itemName:(NSString *)itemname image:(NSString *)image url:(NSString *)url price:(NSString *)price tag:(NSString *)tag delegate:(id<ReqDelegate>)delegate;{
    _delegate = nil;
    _delegate = delegate;
    
    NSMutableDictionary *param = [[NSMutableDictionary alloc] initWithCapacity:0];
    SET_PARAM(itemid, @"id", param);
    SET_PARAM(itemname, @"name", param);
    SET_PARAM(image, @"image", param);
    SET_PARAM(url, @"url", param);
    SET_PARAM(price, @"price", param);
    SET_PARAM(tag, @"tag", param);
    [HttpRequest requestWithURLStr:apiWeiboComment
                             param:param
                        httpMethod:HttpMethodGet
                            isAsyn:YES
                   completionBlock:^(ASIHTTPRequest *request) {
                       NSDictionary *jsonDict = [request.responseString JSONValue];
                        BMLog(@"jsonDict:%@",jsonDict);
                       if (jsonDict) {
                           NSString *itemIdStr = [jsonDict objectForKey:@"itemid"];
                           NSString *weiboIdStr = [jsonDict objectForKey:@"weiboid"];
                           NSArray *comments = [jsonDict objectForKey:@"comments"];
                           NSMutableArray *coms = [[NSMutableArray alloc] initWithCapacity:0];
                           if (isArrayWithCountMoreThan0(comments)) {
                               for (NSDictionary *dic in comments) {
                                   if (isDictWithCountMoreThan0(dic)) {
                                       Comment *com = [Comment createWithDict:dic];
                                       [coms addObject:com];
                                   }
                               }
                           }
                           if ([_delegate respondsToSelector:@selector(finishCommentReq:itemId:weiboId:comments:)]) {
                               [_delegate finishCommentReq:self itemId:itemIdStr weiboId:weiboIdStr comments:coms];
                           }
                       }else{
                           if ([_delegate respondsToSelector:@selector(finishErrorReq:errorCode:message:)]) {
                               [_delegate finishErrorReq:self errorCode:1 message:@"无数据"];
                           }
                       }
                   } failedBlock:^(ASIHTTPRequest *request) {
                       if ([_delegate respondsToSelector:@selector(finishErrorReq:errorCode:message:)]) {
                           [_delegate finishErrorReq:self errorCode:1 message:@"无数据"];
                       }
                   }];

}
@end
