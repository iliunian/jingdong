//
//  HotSourceRequest.m
//  iMiniTao
//
//  Created by liunian on 13-8-16.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import "HotSourceRequest.h"
#import "HttpRequest.h"

#define apiHotSource @"http://imitao.sinaapp.com/api/hotsource/list/"

@implementation HotSourceRequest

- (void)requestHotSourcesWithDelegate:(id<ReqDelegate>)delegate{

    [HttpRequest requestWithURLStr:apiHotSource
                             param:nil
                        httpMethod:HttpMethodGet
                            isAsyn:YES
                   completionBlock:^(ASIHTTPRequest *request) {
                       NSDictionary *jsonDict = [request.responseString JSONValue];
                       
                       if ([HttpRequest isRequestSuccess:request]) {
                           NSMutableArray *rstArray = [NSMutableArray arrayWithCapacity:0];
                           NSArray *sources = [[jsonDict objectForKey:@"data"] objectForKey:@"items"];
                           
                           if (isArrayWithCountMoreThan0(sources)) {
                               for (NSDictionary *dic in sources) {
                                   if (isDictWithCountMoreThan0(dic)) {
                                       HotSource *s = [HotSource createWithDict:dic];
                                       [rstArray addObject:s];
                                   }
                               }
                               
                               if ([delegate respondsToSelector:@selector(finishSourceReq:hotSources:)]) {
                                   [delegate finishSourceReq:self hotSources:rstArray];
                               }
                               
                               
                           }else{
                               if ([delegate respondsToSelector:@selector(finishErrorReq:errorCode:message:)]) {
                                   [delegate finishErrorReq:self errorCode:1 message:@"无数据"];
                               }
                           }
                       }
                   } failedBlock:^(ASIHTTPRequest *request) {
                       if ([delegate respondsToSelector:@selector(finishErrorReq:errorCode:message:)]) {
                           [delegate finishErrorReq:self errorCode:1 message:@"无数据"];
                       }
                   }];

}
@end
