//
//  SourceView.h
//  iMiniTao
//
//  Created by liunian on 13-8-14.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SourceManager.h"

@protocol SourceViewDelegate <NSObject>

@optional
- (void)setting;
- (void)favorit;
- (void)search;
- (void)forward;
@end

@interface SourceView : UIView<SourceManagerDelegate>
{
    int                 _currentPage;
    int                 _totalPage;
    BOOL                _isEditing;
}
@property (nonatomic, assign) id<SourceViewDelegate>delegate;
@property (nonatomic, assign) int      currentPage;
@property (nonatomic, assign) int      totalPage;
@property (nonatomic, assign) BOOL     isEditing;
@property (nonatomic, assign) BOOL     isSourceViewShown;


/*
 *根据页码滑动到当前页面所在区域
 */
- (void)scrollRectToVisibleByPage:(int)page;
- (void)endEditState;
/*
 *SourceView 出现/离开
 */
- (void)sourceViewDidAppear;
- (void)sourceviewWillDisappear;
@end
