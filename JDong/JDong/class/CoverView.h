//
//  CoverView.h
//  JDong
//
//  Created by liu nian on 13-12-12.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CoverView : UIView
{
    NSTimer * _switchTimer;
    BOOL _showDefault;
}

- (void)startImageSwitch;
- (void)stopImageSwitch;
@end
