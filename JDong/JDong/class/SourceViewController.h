//
//  SourceViewController.h
//  iMiniTao
//
//  Created by liunian on 13-8-16.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import "ViewController.h"
@class Source;
@class MeSource;
@protocol SourceViewControllerDelegate <NSObject>
@optional
//若isAdd为真，则为添加频道；否则为删除频道
- (void)handleSource:(Source *)source isAdd:(BOOL)isAdd;

- (void)handleMeSource:(MeSource *)source isAdd:(BOOL)isAdd;
@end

@interface SourceViewController : ViewController
@property (nonatomic, assign) id<SourceViewControllerDelegate>  delegate;
@end
