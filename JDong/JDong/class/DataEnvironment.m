//
//  DataEnvironment.m
//  FlipBoard
//
//  Created by Kevin Huang on 9/15/10.
//  Copyright 2010 iTotem. All rights reserved.
//

#import "DataEnvironment.h"
#import "MainViewController.h"

@implementation DataEnvironment
static DataEnvironment *sharedInst = nil;

@synthesize mainViewController = _mainViewController;
@synthesize navController = _navController;
@synthesize numOfDetailView = _numOfDetailView;
@synthesize isMainViewPresentingController = _isMainViewPresentingController;


+ (id)sharedDataEnvironment 
{
	@synchronized( self ) 
	{
		if ( sharedInst == nil ) 
		{
            sharedInst = [[self alloc] init];
		}
	}
	return sharedInst;
}

- (id)init {
	if ( sharedInst != nil )
    {
        return sharedInst;
	}
    
    self = [super init];
	if (self)
    {
		sharedInst = self;
		[self initData];
	}
	return self;
}

- (void)initData 
{
    _numOfDetailView = 0;
	self.mainViewController = nil;
    self.navController = nil;
    self.isMainViewPresentingController = NO;
}
- (MDSlideNavigationViewController *)currentNavigation
{
        return self.navController;
}
- (NSUInteger)retainCount {
	return NSUIntegerMax;
}

- (oneway void)release {
}

- (id)retain {
	return sharedInst;
}

- (id)autorelease {
	return sharedInst;
}
- (UIViewController *)presentingController
{
    UIViewController *viewController = nil;
    UIViewController *currentController = self.navController;
    while (currentController != nil){
        viewController = currentController;
        currentController = currentController.modalViewController;
    }
    return viewController;
}
@end


