//
//  HotSourceRequest.h
//  iMiniTao
//
//  Created by liunian on 13-8-16.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ReqDelegate.h"
#import "HotSource.h"

@interface HotSourceRequest : NSObject
- (void)requestHotSourcesWithDelegate:(id<ReqDelegate>)delegate;
@end
