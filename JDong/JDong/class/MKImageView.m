//
//  MKImageView.m
//  iCarStyle
//
//  Created by liunian on 13-9-5.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import "MKImageView.h"
#import "AdItem.h"
#import "DataEnvironment.h"
#import "MainViewController.h"
#import "ADWebViewController.h"
@interface MKImageView () <UIGestureRecognizerDelegate>{
    UITapGestureRecognizer *_singleTapGesture;
    UIStatusBarStyle        _savedStatusBarStyle;
}


- (void)handleTapGesture:(UITapGestureRecognizer *)recognizer;
@end

@implementation MKImageView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/
- (void)resetImage {
    self.adItem = nil;
}

#pragma mark - Setter method
- (void)setAdItem:(AdItem *)adItem{
    if (_adItem != adItem) {
        _adItem = adItem;
        
        if (adItem) {
            
            if (_adItem.clickUrl) {
                self.userInteractionEnabled = YES;
                if (!_singleTapGesture) {
                    _singleTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                action:@selector(handleTapGesture:)];
                    _singleTapGesture.delegate = self;
                    _singleTapGesture.numberOfTapsRequired = 1;
                    
                    [self addGestureRecognizer:_singleTapGesture];
                }
            }
            
        } else {
            self.userInteractionEnabled = NO;
            if (_singleTapGesture) {
                [self removeGestureRecognizer:_singleTapGesture];
                RELEASE_SAFELY(_singleTapGesture);
            }
        }
    }
}


#pragma mark - UITapGestureRecognizer Delegate
- (void)handleTapGesture:(UITapGestureRecognizer *)recognizer {
    
    if (recognizer.state == UIGestureRecognizerStateEnded &&
        recognizer == _singleTapGesture) {
        if (!self.image) { //图片没有显示前，点击无效
            return;
        }
        
        if (!_adItem || !_adItem.clickUrl) {
            return;
        }
        
        BOOL clicked = YES;
        
        [[DataEnvironment sharedDataEnvironment].mainViewController hideCover];
        
        if (clicked) {
            NSString *clickTypeStr = @"";
            if (_adItem.clickType == MKAdClickTypeApp) {
                clickTypeStr = @"App";
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:_adItem.clickUrl]];
                
            } else if (_adItem.clickType == MKAdClickTypeUrl) {
                clickTypeStr = @"Url";
                ADWebViewController *webViewController = [[ADWebViewController alloc] initWithURL:[NSURL URLWithString:_adItem.clickUrl]];
                [[DataEnvironment sharedDataEnvironment].navController presentViewController:webViewController animated:YES completion:^{
                    [[DataEnvironment sharedDataEnvironment].mainViewController hideCover];
                }];
                
            }
    
            NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:
                                  _adItem.title, @"title",_adItem.clickUrl, @"clickUrl",clickTypeStr,@"clickType", nil];
            [MobClick event:@"coverClick" attributes:dict];
        }
    }
    
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    
    if ([touch.view isKindOfClass:[UIButton class]]) {
        return NO;
    }
    return YES;
}
@end
