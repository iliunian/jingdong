//
//  MKPageControl.m
//  Mooker
//
//  Created by Jikui Peng on 12-3-15.
//  Copyright (c) 2012年 banma. All rights reserved.
//

#import "MKPageControl.h"

@interface MKPageControl ()

- (void) updateDots;
@end

@implementation MKPageControl
@synthesize normalImage = _normalImage;
@synthesize highlightedImage = _highlightedImage;

- (void)dealloc{
    RELEASE_SAFELY(_normalImage);
    RELEASE_SAFELY(_highlightedImage);
    [super dealloc];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.normalImage = [UIImage imageNamed:@"bg_page_normal.png"];
        self.highlightedImage = [UIImage imageNamed:@"bg_page_select.png"];
    }
    return self;
}

- (void)setNormalImage:(UIImage *)normalImage
{
    _normalImage = normalImage;
    [self updateDots];
}

- (void)setHighlightedImage:(UIImage *)highlightedImage
{
    _highlightedImage = highlightedImage;
    [self updateDots];
}

- (void)updateDots
{
    if(_normalImage || _highlightedImage)
        {
            NSArray *subviews = self.subviews;
            for(NSInteger i = 0;i<subviews.count;i++)
                {
                    if ([[subviews objectAtIndex:i] isKindOfClass:[UIImageView class]]) {
                        UIImageView *dot = [subviews objectAtIndex:i];
                        dot.image = self.currentPage == i ? _highlightedImage : _normalImage ;
                    }
                }
        }
    [self setNeedsDisplay];
}

- (void)setCurrentPage:(NSInteger)currentPage
{
    [super setCurrentPage:currentPage];
    [self updateDots];  
}
@end
