//
//  MKPageControl.h
//  Mooker
//
//  Created by Jikui Peng on 12-3-15.
//  Copyright (c) 2012年 banma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MKPageControl : UIPageControl

@property (nonatomic, retain) UIImage   *normalImage;
@property (nonatomic, retain) UIImage   *highlightedImage;

@end
