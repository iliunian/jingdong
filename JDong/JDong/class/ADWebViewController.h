//
//  ADWebViewController.h
//  JDong
//
//  Created by liunian on 13-12-16.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
@interface ADWebViewController : UIViewController
- (id)initWithURL:(NSURL*)URL;
@end
