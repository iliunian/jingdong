//
//  ItemManager.m
//  JDong
//
//  Created by liunian on 13-12-16.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import "ItemManager.h"
#import "ShortUrlEntity.h"

@interface ItemManager (){
    MKEnvObserverCenter    *_obCenter;
}
@end

static ItemManager * sharedManager;
@implementation ItemManager
+ (ItemManager *)sharedManager{
    if (!sharedManager) {
        sharedManager = [[ItemManager alloc] init];
    }
    return sharedManager;
}
- (id)init
{
    self = [super init];
    if (self) {
        _obCenter = [[MKEnvObserverCenter alloc] init];
    }
    return self;
}

- (void)addObserver:(id<ItemManagerDelegate>)observer
{
    [_obCenter addEnvObserver:observer];
}

- (void)removeObserver:(id<ItemManagerDelegate>)observer
{
    [_obCenter removeEnvObserver:observer];
}

- (void)itemShortUrlWithItemid:(NSInteger)itemid{
    
    if (itemid == 0 ) {
        return;
    }
    
    ShortUrlEntity *uEntity = [self getShortUrlEntityWithItemid:itemid];
    
    if (uEntity) {
        [_obCenter noticeObervers:@selector(mkManagerShortUrlDidFinishedWithShortUrl:)
                       withObject:uEntity.shorturl];
    }else{
        [_obCenter noticeObervers:@selector(mkManagerShortUrlDidFinishedWithShortUrl:)
                       withObject:nil];
    }
    
}
- (ShortUrlEntity *)getShortUrlEntityWithItemid:(NSInteger)itemid{
    ShortUrlEntity  *uEntity = nil;
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"ShortUrlEntity"
                                              inManagedObjectContext:self.managedObjectContext];
    NSPredicate *predicate;
    predicate = [NSPredicate predicateWithFormat:@"(itemid == %d)", itemid];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:entity];
    [fetchRequest setPredicate:predicate];
    [fetchRequest setIncludesPendingChanges:YES];
    
    NSArray * fetchArray = [self.managedObjectContext executeFetchRequest:fetchRequest error:nil];
    
    if ([fetchArray count]>0) {
        uEntity = [fetchArray lastObject];
    }
    return uEntity;
}
- (void)setShortUrl:(NSString *)shortUrl WithItemid:(NSInteger)itemid;{
    if (itemid < 0 || shortUrl == nil || [shortUrl length] <= 0 ) {
        return;
    }
    
    ShortUrlEntity *oldEntity = [self getShortUrlEntityWithItemid:itemid];
    if (oldEntity) {
        [oldEntity setItemid:[NSNumber numberWithInteger:itemid]];
        [oldEntity setShorturl:shortUrl];
        NSError *error = nil;
        if (![self.managedObjectContext save:&error]) {
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
        return;
    }
    ShortUrlEntity *newEntity= [NSEntityDescription insertNewObjectForEntityForName:@"ShortUrlEntity"
                                                                 inManagedObjectContext:self.managedObjectContext];
    
    [newEntity setItemid:[NSNumber numberWithInteger:itemid]];
    [newEntity setShorturl:shortUrl];
    
    NSError *error = nil;
    if (![self.managedObjectContext save:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
}


@end
