//
//  ReqDelegate.h
//  iMiniTao
//
//  Created by liu nian on 13-8-14.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ReqDelegate <NSObject>
@optional
- (void)finishErrorReq:(id)Req errorCode:(int) errorCode message:(NSString *)message;
//Source
- (void)finishSourceReq:(id)Req sources:(NSArray *)sources;
//HotSource
- (void)finishSourceReq:(id)Req hotSources:(NSArray *)hotSources;

//AD
- (void)finishADReq:(id)Req ads:(NSArray *)ads;
- (void)finishCommentReq:(id)Req itemId:(NSString *)itemid weiboId:(NSString *)weiboid comments:(NSArray *)comments;

- (void)finishFeedback:(id)Req;
@end
