//
//  Util.h
//  DayPainting
//
//  Created by nian liu on 12-7-28.
//  Copyright (c) 2012年 banma.com. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum _ALIGNTYPE
{
    ALIGNTYPE_LEFT,
    ALIGNTYPE_CENTER,
    ALIGNTYPE_RIGHT,
}ALIGNTYPE;

@interface Util : NSObject
//NSDate－>NSString
+ (NSString *)dateToNSNSString:(NSDate *)date;
// NSString->NSDate
+ (NSDate *)NSStringToDate:(NSString *)string;

//判断某个点是否在某个区域里
+ (BOOL)isInRect:(CGPoint)p rect:(CGRect)rect;
#pragma mark - Word Counter
/*
 *  计算微博或者用户反馈的输入字符数
 */
+ (int)countWord:(NSString *)s;


+ (BOOL)unfoldUpdateTime;
+ (NSString *)stringWithDate:(NSDate *)date;
#pragma mark - JSON Convert
/*
 *  JSON转换成Dict
 */
//+ (NSDictionary *)convertJSONToDict:(NSString *)string;

/*
 *  Dict转换成JSON
 */
//+ (NSString *)convertDictToJSON:(NSDictionary *)dict;

/*
 *  获取Dictionary中的元素,主要防止服务器发送@""或者obje-c转化成NSNull
 */
+ (id)getElementForKey:(id)key fromDict:(NSDictionary *)dict;
+ (id)getElementForKey:(id)key fromDict:(NSDictionary *)dict forClass:(Class)class;


+ (NSInteger)getAlign:(ALIGNTYPE)type;

#pragma mark - UTF8 Encode
/*
 *  encode UTF8
 */
+ (NSString *)encodeUTF8Str:(NSString *)encodeStr;

#pragma mark - Image Resize
+ (UIImage *)image:(UIImage *)image fitInSize:(CGSize)size;
+ (UIImage *)image:(UIImage *)image centerInSize:(CGSize)size;
+ (UIImage *)image:(UIImage *)image fillInSize:(CGSize)size;
+ (UIImage *)image:(UIImage *)image fitToWidth:(CGFloat)width;
+ (UIImage *)image:(UIImage *)image fitToHeight:(CGFloat)height;
#pragma mark - View to Image
+ (UIImage *)imageByRenderInContextWithView:(UIView *) theView  atFrame:(CGRect)rect;
+ (UIImage *)imageByRenderInContextWithView:(UIView *)view;
+ (void)drawView:(UIView*)view inContext:(CGContextRef)context xOffset:(float)xOffset yOffset:(float)yOffset;
+ (UIImage*)imageByDrawInContextWithView:(UIView*)view;
+ (UIImage*)imageByDrawInContextWithView:(UIView *)view inSize:(CGSize)size;
 
+ (NSString *)getMacAddress;
+ (NSString *)getEncryptMacAddress;

+ (BOOL)validateEmail:(NSString*)email;

+ (NSString *)sha1:(NSString *)str;
+ (NSString *)md5Hash:(NSString *)str;

/**
 * 弹出公共提示框
 */
+ (void)showTipsView:(NSString *)tips;  //普通提示信息，在window弹出
+ (void)showWarningTipsView:(NSString *)tips;   //警告提示信息，在window弹出
+ (void)showErrorTipsView:(NSString *)tips;     //错误提示信息，在window弹出

//自定义提示框的背景色和文本色，在window弹出
+ (void)showTipsView:(NSString *)tips withTextColor:(UIColor *)textColor backColor:(UIColor *)backColor;

//默认颜色在指定的view弹出
+ (void)showTipsView:(NSString *)tips atView:(UIView *)view;
//指定的文本颜色，背景颜色在指定的view弹出
+ (void)showTipsView:(NSString *)tips atView:(UIView *)view withTextColor:(UIColor *)textColor backColor:(UIColor *)backColor;
@end
