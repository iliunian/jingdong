//
//  BUAlertView.m
//  BanmaUtilityPro
//
//  Created by Yan FENG on 10/28/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "BUAlertView.h"

@implementation BUAlertView
+ (void)alertWithMessage:(NSString *)msg 
{
    [self alertWithMessage:msg tag:0 delegate:nil];
}

+ (void)alertWithButtonMessage:(NSString *)msg tag:(int)tag delegate:(id)delegate
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示"
                                                    message:msg   
                                                   delegate:delegate   
                                          cancelButtonTitle:@"取消"
                                          otherButtonTitles:@"确定",nil];
    alert.tag = tag;
    [alert show];  
    [alert release]; 
}

+ (void)alertWithMessage:(NSString *)msg tag:(int)tag delegate:(id)delegate
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示"  
                                                    message:msg   
                                                   delegate:delegate
                                          cancelButtonTitle:@"确定"
                                          otherButtonTitles:nil];
    alert.tag = tag;
    [alert show];  
    [alert release]; 
}


@end
