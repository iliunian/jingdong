//
//  BUAlertView.h
//  BanmaUtilityPro
//
//  Created by Yan FENG on 10/28/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BUAlertView : UIAlertView
+ (void)alertWithMessage:(NSString *)msg;
+ (void)alertWithMessage:(NSString *)msg tag:(int)tag delegate:(id)delegate;
+ (void)alertWithButtonMessage:(NSString *)msg tag:(int)tag delegate:(id)delegate;
@end
