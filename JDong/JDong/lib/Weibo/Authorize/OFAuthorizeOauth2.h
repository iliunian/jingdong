//
//  OFAuthorizeOauth2.h
//  PUDemo
//
//  Created by JK.Peng on 13-2-22.
//  Copyright (c) 2013年 njut. All rights reserved.
//

#import "OFAuthorize.h"
#import "OFAuthorizeParseResult.h"
#import "OFRequest.h"

@class OFAuthorizeOauth2;

@protocol OFAuthorizeOauth2DataSource <NSObject>
@required
- (NSString *)authorizeOauth2AuthorizeURL:(OFAuthorizeOauth2 *)authorize;
- (OFRequest *)authorizeOauth2WBRequestForAccessToken:(OFAuthorizeOauth2 *)authorize authorizeCode:(NSString *)code;
@end

@protocol OFAuthorizeOauth2Delegate <NSObject>
@required
- (OFAuthorizeParseResult *)authorizeOauth2:(OFAuthorizeOauth2 *)authorize parseAuthorizeCode:(NSString *)string;
- (BOOL)authorizeOauth2:(OFAuthorizeOauth2 *)authorize parseAccessToken:(NSString *)string;
@end

@interface OFAuthorizeOauth2 : OFAuthorize{
    
    id<OFAuthorizeOauth2Delegate> _dataDelegate;
    id<OFAuthorizeOauth2DataSource> _dataSource;
}

@property (nonatomic,retain) id<OFAuthorizeOauth2Delegate> dataDelegate;
@property (nonatomic,retain) id<OFAuthorizeOauth2DataSource> dataSource;

@end
