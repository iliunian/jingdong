//
//  OFAuthorize.h
//  PUDemo
//
//  Created by JK.Peng on 13-2-22.
//  Copyright (c) 2013年 njut. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OFAuthorizeWebView.h"
#import "OFRequest.h"

@class OFAuthorize;

@protocol OFAuthorizeDelegate <NSObject>
@required
- (void)authorizeDidSuccee:(OFAuthorize *)authorize;
- (void)authorize:(OFAuthorize *)authorize didFailWithError:(NSError *)error;
@end

@interface OFAuthorize : NSObject<OFRequestDelegate,OFAuthorizeWebViewDelegate,UIAlertViewDelegate>{
    
    id<OFAuthorizeDelegate> _delegate;
    OFAuthorizeWebView * _webViewController;
    OFRequest   *_request;

}
@property (nonatomic, retain) OFRequest *request;
@property (nonatomic, retain) id<OFAuthorizeDelegate> delegate;
@property (nonatomic, retain) OFAuthorizeWebView *webViewController;

- (void)startAuthorize;
- (void)handleAuthorizeCancel;
- (void)handleAuthorizeSuccess;
- (void)handleAuthorizeFailWithError:(NSError*)error;
- (void)resetWebView;
- (void)resetOFRequest;


@end
