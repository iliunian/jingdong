//
//  OFAuthorizeParseResult.h
//  PUDemo
//
//  Created by JK.Peng on 13-2-22.
//  Copyright (c) 2013年 njut. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OFAuthorizeParseResult : NSObject{
    NSString * _oauthToken;
    NSString * _oauthTokenSecret;
    NSString * _authorizeCode;
    BOOL _authorizeErrorCode;
}

@property (nonatomic,retain) NSString * oauthToken;
@property (nonatomic,retain) NSString * oauthTokenSecret;
@property (nonatomic,retain) NSString * authorizeCode;
@property (nonatomic,assign) BOOL authorizeErrorCode;

@end
