//
//  OFAuthorizeParseResult.m
//  PUDemo
//
//  Created by JK.Peng on 13-2-22.
//  Copyright (c) 2013年 njut. All rights reserved.
//

#import "OFAuthorizeParseResult.h"

@implementation OFAuthorizeParseResult
@synthesize oauthToken = _oauthToken;
@synthesize oauthTokenSecret = _oauthTokenSecret;
@synthesize authorizeCode = _authorizeCode;
@synthesize authorizeErrorCode = _authorizeErrorCode;

- (void)dealloc
{
    [_oauthToken release], _oauthToken = nil;
    [_oauthTokenSecret release], _oauthTokenSecret = nil;
    [_authorizeCode release], _authorizeCode = nil;
    [super dealloc];
}

@end
