//
//  OFEngineManager.h
//  PUDemo
//
//  Created by JK.Peng on 13-2-22.
//  Copyright (c) 2013年 njut. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WBEngineSina.h"
#import "WBEngineTX.h"
#import "QZoneEngine.h"

@interface OFEngineManager : NSObject
+ (WBEngineSina *)getWBEngineSinaWithAppKey:(NSString *)appKey appSecret:(NSString *)appSecret;
+ (WBEngineTX *)getWBEngineTXWithAppKey:(NSString *)appKey appSecret:(NSString *)appSecret;
+ (QZoneEngine *)getQZoneEngineWithAppId:(NSString *)appId appKey:(NSString *)openkey;

@end
