//
//  OFEngineManager.m
//  PUDemo
//
//  Created by JK.Peng on 13-2-22.
//  Copyright (c) 2013年 njut. All rights reserved.
//

#import "OFEngineManager.h"

@implementation OFEngineManager

+ (WBEngineSina *)getWBEngineSinaWithAppKey:(NSString *)appKey appSecret:(NSString *)appSecret
{
    return [[[WBEngineSina alloc] initWithAppKey:appKey appSecret:appSecret] autorelease];
}

+ (WBEngineTX *)getWBEngineTXWithAppKey:(NSString *)appKey appSecret:(NSString *)appSecret
{
    return [[[WBEngineTX alloc] initWithAppKey:appKey appSecret:appSecret] autorelease];
}

+ (QZoneEngine *)getQZoneEngineWithAppId:(NSString *)appId appKey:(NSString *)openkey
{
    return [[[QZoneEngine alloc] initWithAppId:appId appKey:openkey] autorelease];
}
@end
