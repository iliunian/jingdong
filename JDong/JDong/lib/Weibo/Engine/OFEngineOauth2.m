//
//  OFEngineOauth2.m
//  PUDemo
//
//  Created by JK.Peng on 13-2-22.
//  Copyright (c) 2013年 njut. All rights reserved.
//

#import "OFEngineOauth2.h"
#import "SFHFKeychainUtils.h"

#define kWBKeychainOauth2AccessToken [NSString stringWithFormat:@"%@_WeiBoOauth2AccessToken",[self urlSchemeString]]
#define kWBKeychainOauth2ExpireTime  [NSString stringWithFormat:@"%@_WeiBoOauth2ExpireTime",[self urlSchemeString]]

@implementation OFEngineOauth2

@synthesize accessToken = _accessToken;
@synthesize expireTime = _expireTime;

- (id)initWithAppKey:(NSString *)theAppKey appSecret:(NSString *)theAppSecret type:(OFTypeE)type
{
    self = [super initWithAppKey:theAppKey appSecret:theAppSecret oauthVer:kOFOauth2 type:type];
    if (self) {
    }
    return self;
}

- (void)dealloc
{
    [_accessToken release], _accessToken = nil;
    [super dealloc];
}

- (OFRequest *)generateRequestWithUrl:(NSString *)url
                           httpMethod:(NSString *)httpMethod
                               params:(NSMutableDictionary *)params
                         postDataType:(OFRequestPostDataType)postDataType
                     httpHeaderFields:(NSDictionary *)httpHeaderFields
                             tokenKey:(NSString *)tokenKey
                             delegate:(id<OFRequestDelegate>)delegate;
{
    if (_accessToken) {
        [params setObject:_accessToken forKey:tokenKey];
    }
    
    return [OFRequest requestWithURL:url
                          httpMethod:httpMethod
                              params:params
                        postDataType:postDataType
                    httpHeaderFields:httpHeaderFields
                            delegate:delegate];
}

#pragma makr - OFAuthorizeDelegate
- (void)authorizeDidSuccee:(OFAuthorize *)authorize
{
    OFAuthorizeOauth2 * auth = (OFAuthorizeOauth2 *)self.authorize;
    auth.dataSource = nil;
    auth.dataDelegate = nil;
    [super authorizeDidSuccee:authorize];
}

- (void)authorize:(OFAuthorize *)authorize didFailWithError:(NSError *)error
{
    OFAuthorizeOauth2 * auth = (OFAuthorizeOauth2 *)self.authorize;
    auth.dataSource = nil;
    auth.dataDelegate = nil;
    [super authorize:authorize didFailWithError:error];
}

#pragma mark - OFAuthorizeOauth2DataSource
- (NSString *)authorizeOauth2AuthorizeURL:(OFAuthorizeOauth2 *)authorize
{
    // Should be implemented by subclasses
    return nil;
}

- (OFRequest *)authorizeOauth2WBRequestForAccessToken:(OFAuthorizeOauth2 *)authorize authorizeCode:(NSString *)code
{
    // Should be implemented by subclasses
    return nil;
}

#pragma mark - OFAuthorizeOauth2Delegate
- (OFAuthorizeParseResult *)authorizeOauth2:(OFAuthorizeOauth2 *)authorize parseAuthorizeCode:(NSString *)string
{
    // Should be implemented by subclasses
    return nil;
}

- (BOOL)authorizeOauth2:(OFAuthorizeOauth2 *)authorize parseAccessToken:(NSString *)string
{
    // Should be implemented by subclasses
    return NO;
}

#pragma mark - Overwrite
- (BOOL)isLoggedIn
{
    return (_accessToken && ![self isAuthorizeExpired]);
}

- (BOOL)isAuthorizeExpired
{
    if ([[NSDate date] timeIntervalSince1970] > self.expireTime)
    {
        // force to log out
        [self deleteAuthorizeData];
        return YES;
    }
    return NO;
}

- (void)startAuthorize
{
    OFAuthorizeOauth2 * auth2 = [[[OFAuthorizeOauth2 alloc] init] autorelease];
    auth2.delegate = self;
    auth2.dataSource = self;
    auth2.dataDelegate = self;
    self.authorize = auth2;
    [self.authorize startAuthorize];
}

- (void)saveAuthorizeData
{
    [[NSUserDefaults standardUserDefaults] setObject:_accessToken forKey:kWBKeychainOauth2AccessToken];
    [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%lf", _expireTime]
                                              forKey:kWBKeychainOauth2ExpireTime];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [super saveAuthorizeData];
}

- (void)readAuthorizeData
{
    self.accessToken = [[NSUserDefaults standardUserDefaults] objectForKey:kWBKeychainOauth2AccessToken];
    self.expireTime = [[[NSUserDefaults standardUserDefaults] objectForKey:kWBKeychainOauth2ExpireTime] doubleValue];
    [super readAuthorizeData];
}

- (void)deleteAuthorizeData
{
    self.accessToken = nil;
    self.expireTime = 0;
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kWBKeychainOauth2AccessToken];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kWBKeychainOauth2ExpireTime];
    [super deleteAuthorizeData];
}
@end

