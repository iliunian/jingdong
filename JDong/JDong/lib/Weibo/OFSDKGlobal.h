//
//  OFSDKGlobal.h
//  PUDemo
//
//  Created by JK.Peng on 13-2-22.
//  Copyright (c) 2013年 njut. All rights reserved.
//

#ifndef PUDemo_OFSDKGlobal_h
#define PUDemo_OFSDKGlobal_h

#define kOFSDKErrorDomain           @"WeiBoSDKErrorDomain"
#define kOFSDKErrorCodeKey          @"WeiBoSDKErrorCodeKey"

#define kOFSDKAPIDomain             @"https://api.weibo.com/2/"

typedef enum
{
	kOFErrorCodeInterface	= 100,
	kOFErrorCodeSDK         = 101,
    kOFErrorCodeAuthorizeClass        = 102,       // 错误的WBAuthorize子类实例，功能不支持
    kOFErrorCodeWBEngineClass         = 103       // 错误的WBEngine子类实例，功能不支持
}OFErrorCode;

typedef enum
{
	kOFSDKErrorCodeParseError       = 200,
	kOFSDKErrorCodeRequestError     = 201,
	kOFSDKErrorCodeAccessError      = 202,
	kOFSDKErrorCodeAuthorizeError	= 203,
}OFSDKErrorCode;

typedef enum
{
    kOFTypeSina   = 0,    // 新浪微博
    kOFTypeTX     = 1,      // 腾讯微博
    kOFTypeQZone  = 2    // QQ空间
}OFTypeE;

typedef enum
{
    kOFOauth1,      // Oauth1.0
    kOFOauth2       // Oauth2.0
}OFOauthVer;

typedef enum {
    kOFRepostCommentNone = 0,
    kOFRepostCommentCurrent = 1,
    kOFRepostCommentOriginal = 2,
    kOFRepostCommentAll = 3
}OFRepostCommentType;

#endif
