//
//  UIImage+UIImage_Gray.m
//  BMTV
//
//  Created by Tang Haibo on 11-11-8.
//  Copyright (c) 2011年 __MyCompanyName__. All rights reserved.
//

#import "UIImage+UIImage_Gray.h"

@implementation UIImage (UIImage_Gray)
- (UIImage *)grayImage{
    CGRect imageRect = CGRectMake(0, 0, self.size.width, self.size.height);
	
    // 创建gray的CGColorSpaceRef
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceGray();
	
    // 创建CGContextRef
    CGContextRef context = CGBitmapContextCreate(nil, self.size.width, 
                                                 self.size.height, 
                                                 8, 
                                                 0, 
                                                 colorSpace, 
                                                 kCGImageAlphaNone);
	
    CGContextDrawImage(context, imageRect, [self CGImage]);
	
    CGImageRef imageRef = CGBitmapContextCreateImage(context);
    
    UIImage *newImage = [UIImage imageWithCGImage:imageRef];
	
    CGColorSpaceRelease(colorSpace);
    CGContextRelease(context);
    CFRelease(imageRef);
    
    return newImage;
}
@end
