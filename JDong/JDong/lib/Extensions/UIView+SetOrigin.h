//
//  UIView+SetOrigin.h
//  LimitFreeMaster
//
//  Created by Haibo Tang on 11-8-24.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface UIView (UIView_Frame)
- (void)setOrigin:(CGPoint)origin;
- (CGFloat)setOriginY:(CGFloat)y;

- (void)setSize:(CGSize)size;
- (void)setWidth:(CGFloat)width;
- (void)setHeight:(CGFloat)height;

- (float)width;
- (float)height;
- (CGSize)size;
- (float)rightx;
- (float)bottomy;

- (void)safeSetCenter:(CGPoint)center;
@end
