//
//  Config.h
//  iMiniTao
//
//  Created by liunian on 13-8-14.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#ifndef iMiniTao_Config_h
#define iMiniTao_Config_h


#define cuzyAppkey      @"200075"
#define cuzyAppSecret   @"54b09b3306ab0f6a1a3a70342701b40c"

// ************************************//
// **************   渠道列表  ***********//
// ************************************//
#define kUmengAppstoreChannelId       @"appstore"
#define kUmeng91storeChannelId       @"91市场"
#define kUmengTongbustoreChannelId       @"tongbu"
#define kUmengWeiphoneChannelId       @"weiphone"
// ************************************//

#define kUmengChannelId       @"appstore"
#define kUmengAppKey        @"52a803e956240b903201148f"
#define kWeChatAPPID        @"wx94b2f5970080a777"
#define kWeiboAppKey        @"1934477254"

#define kLinkItunes @"https://itunes.apple.com/app/id776251804?mt=8"
#define kAPPID  776251804

// ************************************//
// ********  Save DIR Base  ***********//
// ************************************//
#define MKDirFileCache [[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@"FileCache"]
#define MKCacheDir     [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0]
#define MKDocumentDir  [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0]

#define kSelectedThemID @"1005"
#define COLOR_RGB_NAV        [UIColor getColor:@"E33C3E"]

#define COLOR_BG_VIEW       [UIColor colorWithPatternImage:IMGNAMED(@"bg_sourceview.png")]
#define COLOR_RGB_NAV_NIGHT  [UIColor colorWithRed:51.0/255.0f green:51.0/255.0f blue:51.0/255.0f alpha:1.0]

#define COLOR_RGB_BOTTOM     [UIColor colorWithRed:234.0/255.0f green:234.0/255.0f blue:234.0/255.0f alpha:1.0]
#define COLOR_RGB_BOTTOM_NIGHT  [UIColor colorWithRed:51.0/255.0f green:51.0/255.0f blue:51.0/255.0f alpha:1.0]
#define COLOR_BG_BOTTOM     [UIColor getColor:@"CD2A2C"]

#endif
