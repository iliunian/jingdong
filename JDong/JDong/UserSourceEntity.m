//
//  UserSourceEntity.m
//  JDong
//
//  Created by liunian on 13-12-10.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import "UserSourceEntity.h"


@implementation UserSourceEntity

@dynamic desc;
@dynamic icon;
@dynamic isCustom;
@dynamic name;
@dynamic keyword;
@dynamic sid;
@dynamic sortid;
@dynamic themeid;
@dynamic itemtype;
@dynamic url;
@end
