//
//  CheckVersion.h
//  yeeyan
//
//  Created by liunian on 13-7-26.
//  Copyright (c) 2013年 liunian. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol VersionDelegate <NSObject>

@optional
- (void)noNewVersion;
- (void)hasNewVersion;
@end

@interface CheckVersion : NSObject

+(CheckVersion *)sharedCVInstance;
- (void)checkVersionOfServerWithDelegate:(id<VersionDelegate>)delegate;
@end
